#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include "evpTestes.c"

#define MAX_MSG 1024
#define MAX_MSG128 16
#define MAX_SIZE_chave128 16
#define MAX_SIZE_chave192 24
#define MAX_SIZE_chave256 32

void* trataConversa(void *);
void arquivo(int*);

typedef struct {
	int socket_desc, set;
}thread_arg;

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int decrip(unsigned char *textocifrado, unsigned char *textoclaro, int tamanho){
    // /* A 256 bit chave */
    unsigned char *chave = (unsigned char *)"01234567890123456789012345678901";

    /* Chave de 192 bits */
    // unsigned char *chave = (unsigned char *)"ARIAEFGHIJ12345678912345";

    // /* Chave de 128 bits */
    // unsigned char *chave = (unsigned char *)"ARIAEFGHIJ123456";

    // /* A 128 bit IV */
    // unsigned char *iv = (unsigned char *)"0123456789012345";

    /* A 64 bit IV */
    unsigned char *iv = (unsigned char *)"01234567";



    int textoclaro_tamanho;

    /* Descifrando textocifrado */
    textoclaro_tamanho = decrypt(textocifrado, tamanho, chave, iv,
                                textoclaro);

    textoclaro[textoclaro_tamanho] = '\0';

    /* Mostrando o texto claro */
    printf("Texto Claro:\n");
    printf("%s\n", textoclaro);

    return 0;

}

int crip (unsigned char *textoclaro, unsigned char *textocifrado){

    // /* A 256 bit chave */
    unsigned char *chave = (unsigned char *)"01234567890123456789012345678901";

    /* Chave de 192 bits */
    // unsigned char *chave = (unsigned char *)"ARIAEFGHIJ12345678912345";

    // /* Chave de 128 bits */
    // unsigned char *chave = (unsigned char *)"ARIAEFGHIJ123456";

    // /* A 128 bit IV */
    // unsigned char *iv = (unsigned char *)"0123456789012345";

    /* A 64 bit IV */
    unsigned char *iv = (unsigned char *)"01234567";
    
    int  textocifrado_tamanho;

    /* Criptografar o textoclaro */
    textocifrado_tamanho = encrypt (textoclaro, strlen ((char *)textoclaro), chave, iv,
                              textocifrado);

    /* Imprimindo texto cifrado */
    printf("Texto Cifrado :\n");
    BIO_dump_fp (stdout, (const char *)textocifrado, textocifrado_tamanho);

    return 0;
}

void arquivo(int *ref){
	int count = 0;

	system("figlet 'Teste Blowfish Criptogrando 4096 bits' >> Dados_BF/BF_256.txt");
	system("date '+%A-feira dia %d de %B de %Y'  >> Dados_BF/BF_256.txt");
	system("date '+Horário Início %T' >> Dados_BF/BF_256.txt" );
		
	while(*ref){
		system("echo {\"memoria\" : `smem -m -p - t -c rss | tail -1| sed '1s/%/ /'` , >> Dados_BF/BF_256.txt");
		system("upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep 'percentage' | sed 's/ //g' | sed 's/[ ]\\+/ /g' | sed '1s/percentage/\"bateria\" /' | sed '1s/:/: /' | sed '1s/%/ ,/' >> Dados_BF/BF_256.txt ");
		system("NUMCPUS=`grep ^proc /proc/cpuinfo | wc -l`; FIRST=`cat /proc/stat | awk '/^cpu / {print $5}'`; sleep 1; SECOND=`cat /proc/stat | awk '/^cpu / {print $5}'`; USED=`echo 2 k 100 $SECOND $FIRST - $NUMCPUS / - p | dc`; echo \"cpu\" : ${USED}} , >> Dados_BF/BF_256.txt");
		
		// system("echo `cat Dados_AES/teste.dat | tr '\n' ' ' | sed '1s/memoria/\"memoria\" /' | sed '1s/cpu/\"cpu\" /'` > Dados_AES/teste.dat");
		// FILE *arq;
		// char *mensagem = (char *) malloc(MAX_MSG * sizeof(char));
		// int result;
		// arq = fopen("Dados_AES/teste.dat", "rt");
		// if (arq == NULL)  // Se houve erro na abertura
		//   {
		//      printf("Problemas na abertura do arquivo\n");
		//      return;
		//   }

		// result = fread (&mensagem[0], sizeof(char), MAX_MSG, arq);
		// printf("Dados Cap : %s\n", mensagem);
		// fclose(arq);
	}

	system("date '+Horário Fim %T' >> Dados_BF/BF_256.txt");
	system("date '+%A-feira dia %d de %B de %Y'  >> Dados_BF/BF_256.txt");
	// system("ETIME=`expr $end - $start`");
	// system("echo -n Tempo de exercução: ${ETIME} >> Dados_BF/BF_256.txt");
	printf("Cliente finalizado com sucesso!\n");

	*ref = -1;
	pthread_exit(NULL);
}



int main(int argc , char *argv[]){
	// variaveis
	int socket_desc;
	struct sockaddr_in servidor;
	int conv;

	/*****************************************/
	/* Criando um socket */
	// AF_INET = ARPA INTERNET PROTOCOLS
	// SOCK_STREAM = TCP
	// 0 = protocolo IP
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
     
	if (socket_desc == -1){
		printf("Nao foi possivel criar socket\n");
		return -1;	
	}

	/* Informacoes para conectar no servidor */
	// IP do servidor
	// familia ARPANET
	// Porta - hton = host to network short (2bytes)
	servidor.sin_addr.s_addr = inet_addr("127.0.0.1");
	servidor.sin_family = AF_INET;
	servidor.sin_port = htons( 1234 );

	//Conectando no servidor remoto
	if (connect(socket_desc , (struct sockaddr *)&servidor , sizeof(servidor)) < 0){
		printf("Nao foi possivel conectar\n");
		return -1;
	}
	printf("Conectado no servidor\n");

	pthread_t infosCliente[2];
	thread_arg *args = (thread_arg *)malloc(sizeof(thread_arg));
	printf("Socket %d\n",socket_desc );
	args->socket_desc = socket_desc;
	printf("Socket depois da passgem: %d\n", args->socket_desc );
	args->set= 1;
	/**** Criando thread para coleta de dados durante a realização das tarefas do cliente******/
	if (pthread_create(&infosCliente[0], NULL, arquivo, &(args->set)) < 0)
	{
		perror("nao foi possivel criar thread: ");
		return -1;
	}{
		printf("Criou thread 1\n");
	}
	/**** Criando thread para comunicação com o servidor******/
	if (pthread_create(&infosCliente[1], NULL, trataConversa, (void*)args) < 0)
	{
		perror("nao foi possivel criar thread: ");
		return -1;
	}{
		printf("Criou thread 2\n");
	}
	
	while(args->set>=0){}
	

	close(socket_desc);
	return 0;
}
// int socket_desc, int *set
void *trataConversa(void *args){
	/*******COMUNICAO - TROCA DE MENSAGENS **************/
	// ptr_thread_arg arg = (thread_arg*) args;
	// printf("Socket %d\n", ((thread_arg*) args)->socket_desc );
	// printf("SET : %d\n", ((thread_arg*) args)->set);
	unsigned char *mensagem  = (char*) malloc(MAX_MSG128*sizeof(char));
	char resposta_servidor[MAX_MSG128];
	char *msg;
	int tamanho;
	
	//Recebe mensagem de boas vindas
	if((tamanho = recv(((thread_arg*) args)->socket_desc, resposta_servidor, MAX_MSG128 , 0)) < 0){
		printf("Falha ao receber resposta\n");
		return -1;
	}
	resposta_servidor[tamanho] = '\0';
	
	puts(resposta_servidor);
	int count = 0;
	while(count++ < 32){
		//Envia mensagem para o servidor
		//AES 128bits o bloco
		// msg =  "Ola, tudo bem?";
		//Blowfish 64bits o bloco
		msg =  "BRASIL 20";
		crip(msg, mensagem);
		printf("Enviando: ... \n");
		if( send(((thread_arg*) args)->socket_desc , mensagem , strlen(mensagem) , 0) < 0){
			printf("Erro ao enviar mensagem\n");
			return -1;
		}

		sleep(1);
		// //Recebendo resposta do servidor (echo)
		// if( (tamanho = recv(socket_desc, resposta_servidor, MAX_MSG128 , 0)) < 0){
		// 	printf("Falha ao receber resposta\n");
		// 	return -1;
		// }
		// printf("Resposta: ");
		// resposta_servidor[tamanho] = '\0';
		// puts(resposta_servidor);

	}

	((thread_arg*) args)->set=0;
	pthread_exit(NULL);
	
	// return 1;

}