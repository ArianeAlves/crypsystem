#include <bson.h>
#include <mongoc.h> 
#include <stdio.h> 
#include <time.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>

#define MAX_MSG 56
#define MAX_BLOCK_64 8
#define MAX_BLOCK_128 16
#define MAX_BLOCK_192 24
#define MAX_BLOCK_256 32

bson_t* formataJson(char *);
int inicioInsert(char *);
int consultarMediaRecursos();
int insertMongo(bson_t *);
struct tm dataNow();

char* initCrip(char*, int, char* );
char* initDecrip(char*, int, int, char*);
int encrypt(unsigned char *, int, unsigned char *, unsigned char *, unsigned char *, char*, int);
int decrypt(unsigned char *, int , unsigned char *, unsigned char *, unsigned char *, char*, int);
char* verificaRecursos();
void handleErrors();
void imprimeTextoCifrado(char *);

struct tm dataNow(){
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  // tm.tm_year += 1900;
  // tm.tm_mon += 1;
  tm.tm_hour-=3;
  //printf("now: %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1,tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
  return tm;
}

bson_t* formataJson(char *texto){
   bson_error_t error ;
   bson_t      * bson ;
   //char *string;
   printf("UTF-8  %s\n", ( const uint8_t * ) texto);
   bson = bson_new_from_json (( const uint8_t * ) texto , strlen(texto), & error );
   // string = bson_as_canonical_extended_json (bson, NULL);
   // printf ("Oiii %s\n", string);
   // bson_free (string);
   return bson;
}

int insertMongo(bson_t *doc){
    mongoc_client_t * client ;
    mongoc_collection_t * collection ;
    bson_error_t error ;
    bson_oid_t oid ;
    struct tm tm;

    mongoc_init ();
    printf("insert Inicializou\n");
    client = mongoc_client_new ( "mongodb://localhost:27017/" );
    collection = mongoc_client_get_collection ( client , "dbRecursosCliente" , "recursos" );
    
    tm = dataNow();
    bson_append_date_time (doc, "data", -1, mktime (&tm) * 1000);

    if ( ! mongoc_collection_insert (
           collection ,MONGOC_INSERT_NONE, doc , NULL, & error )) {
         printf ("%s \n " , error.message );
         return 0;
    }else{

       bson_destroy ( doc );
       mongoc_collection_destroy ( collection );
       mongoc_client_destroy ( client );
       mongoc_cleanup ();
       return 1;
    }
   

}

int consultarMediaRecursos(double dado[], int tamanho){
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *cursor;
  bson_error_t error;
  const bson_t *doc;
  bson_t *query;
  bson_t *query1;
  bson_t *query2;
  time_t segundos;
  time(&segundos);
  struct tm tm; 
  char *str;
  char *string;
  mongoc_init ();
  long int horas =0;
  
  client = mongoc_client_new ( "mongodb://127.0.0.1:27017" );
  collection = mongoc_client_get_collection ( client , "dbRecursosCliente" , "recursos" );
  tm = dataNow();
  //bson_append_date_time (query1, "data", -1, mktime (&tm) * 1000);
  //bson_append_date_time (query1, "date", -1, mktime(&tm)*1000);
  //bson_append_date_time (query2, "$date", -1, (segundos-7200)*1000);
  //bson_append_timestamp (b, key, (int) strlen (key), val, inc)
  //str = bson_as_canonical_extended_json (query1, NULL);
  //printf("%s\n", str);

  //intervalo de 5 minutos
  horas = (mktime(&tm));
  printf("Timer da media: %ld\n", (horas-300)*1000);
  query = BCON_NEW("pipeline", "[", 
    "{","$match", "{","data", 
      "{","$gte", BCON_DATE_TIME((horas-300)*1000), 
      "$lt", BCON_DATE_TIME((horas)*1000), "}",
   "}", "}",
   "{" ,"$group", 
      "{", "_id" , "mediasRecursos", 
         "mediamemoria", "{" ,"$avg", "$memoria","}", 
        "mediabateria", "{", "$avg","$bateria","}",         
         "mediacpu",  "{" , "$avg" , "$cpu","}",
      "}", 
   "}",
   "]");

  //string = bson_as_canonical_extended_json (query, NULL);
  //printf("%s\n", string);
  
  int i=0;
  cursor = mongoc_collection_aggregate (collection, MONGOC_QUERY_NONE, query, NULL, NULL);
  while (mongoc_cursor_next (cursor, &doc)==1){
    str =  bson_array_as_json(doc, NULL);
    printf("Verificação %s\n", str);
    char delim[] = ",";
    char *ptr = strtok(str, "[ \"mediasRecursos\", ");
    
    while (ptr != NULL && i < tamanho)
    {
      dado[i] = atof(ptr);
      printf("%lf\n", dado[i++]);
      //printf(" %d '%s'\n",i++, ptr);
      ptr = strtok(NULL, ", ]");
      
    }
     //bson_as_canonical_extended_json tranforma em json
      
    bson_free (str);
  }

  if (mongoc_cursor_error (cursor, &error)) {
    fprintf (stderr, "Cursor Failure: %s\n", error.message);
  }

  bson_destroy (query);
  mongoc_cursor_destroy (cursor);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
  mongoc_cleanup ();

  if(i>=tamanho){
    return 1;
  }
  return 0;
}

int inicioInsert(char *texto){  
   bson_error_t error ;
   bson_t      * bson ;

   //texto = "{ \"memoria\" : 89.4 , \"bateria\": 67.0 , \"cpu\" : 69.5 }";
   bson = formataJson(texto);

   if ( ! bson ) {
      fprintf ( stderr , "Erro ao transformar em Json%s \n " , error.message );
      return EXIT_FAILURE ;
   }else{
      
      if (insertMongo(bson) == 1){
         printf("==OK INSERT==\n");
      }
   
   }

   return 0;
}


//////////////////////Criptografar ////////////////////



void handleErrors(void){
     ERR_print_errors_fp(stderr);
     abort();
}

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *chave,
             unsigned char *iv, unsigned char *ciphertext, char *criptografia, int tamanhoBlocoAtual){
  EVP_CIPHER_CTX *ctx;
  int len;
  int ciphertext_len;

  /* Criando e inicializando o contexto*/
  if(!(ctx = EVP_CIPHER_CTX_new()))
    handleErrors();
  EVP_CIPHER_CTX_init(ctx);
  
  /*Inicializando o processo de criptografia do texto claro,
   *e escolhendo a criptografia recebida.
   */
  //printf("\033[1;41m %s %d \033[0m\n",newCriptografia, *tamanhoBlocoAtual );
  printf("\033[1;42m Iniciando a criptografia: %s \033[0m\n", criptografia);

  if(strcmp(criptografia, "EVP_bf_cbc()") == 0){
    if(1 != EVP_EncryptInit_ex(ctx, EVP_bf_cbc(), NULL, chave, iv))
      handleErrors();
  }else if(strcmp(criptografia, "EVP_aes_128_cbc()") == 0){
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, chave, iv))
      handleErrors();
  }else if(strcmp(criptografia, "EVP_aes_192_cbc()") == 0){
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_192_cbc(), NULL, chave, iv))
      handleErrors();
  }else if(strcmp(criptografia, "EVP_aes_256_cbc()")== 0){
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, chave, iv))
      handleErrors();
  }else{
    printf("Erro\n");
    handleErrors();
  }
   // EVP_CIPHER_CTX_set_padding(ctx, tamanhoBlocoAtual);
  /*
  * Provide the message to be encrypted, and obtain the encrypted output.
  * EVP_EncryptUpdate can be called multiple times if necessary
  */
  if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
    handleErrors();
  ciphertext_len = len;

  /*
  * Finalise the encryption.  Further ciphertext bytes may be written at
  * this stage.
  */
  if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
    handleErrors();
  ciphertext_len += len;

  /* Clean up */
  EVP_CIPHER_CTX_free(ctx);

  if(strlen(ciphertext) < tamanhoBlocoAtual){
    ciphertext = '\0';
    encrypt(plaintext, plaintext_len, chave,
          iv, ciphertext, criptografia,tamanhoBlocoAtual);
  }
  //imprimeTextoCifrado(ciphertext);
  return ciphertext_len;
 }


int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *chave,
             unsigned char *iv, unsigned char *plaintext, char * criptografia, int tamanhoBlocoAtual){

     EVP_CIPHER_CTX *ctx;

     int len;

     int plaintext_len;

     /* Create and initialise the context */
     if(!(ctx = EVP_CIPHER_CTX_new()))
         handleErrors();
    EVP_CIPHER_CTX_init(ctx);
    //EVP_CIPHER_CTX_set_padding(ctx, 1);
     /*
      * Initialise the decryption operation. IMPORTANT - ensure you use a chave
      * and IV size appropriate for your cipher
      * In this example we are using 256 bit AES (ie a 256 bit chave).  The
      * IV size for *most* modes is the same as the block size.  For AES this
      * is 128 bits EVP_aes_256_cbc()
      */
    
    printf("\033[1;42m Iniciando a Decriptografia: %s Tamanho: %d \033[0m\n", criptografia, ciphertext_len);

    if(strcmp(criptografia, "EVP_bf_cbc()") == 0){
      if(1 != EVP_DecryptInit_ex(ctx, EVP_bf_cbc(), NULL, chave, iv))
          handleErrors();
    }else if(strcmp(criptografia, "EVP_aes_128_cbc()") == 0){
      if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, chave, iv))
        handleErrors();
    }else if(strcmp(criptografia, "EVP_aes_192_cbc()") == 0){
      if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_192_cbc(), NULL, chave, iv))
        handleErrors();
    }else if(strcmp(criptografia,"EVP_aes_256_cbc()") == 0){
      if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, chave, iv))
        handleErrors();
    }else{
      printf("Erro\n");
          handleErrors();

    }
    EVP_CIPHER_CTX_set_padding(ctx, tamanhoBlocoAtual);
     /*
      * Provide the message to be decrypted, and obtain the plaintext output.
      * EVP_DecryptUpdate can be called multiple times if necessary.
      */
     if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, strlen(ciphertext)))
         handleErrors();
     plaintext_len = len;

     /*
      * Finalise the decryption.  Further plaintext bytes may be written at
      * this stage.
      */

    int resull=0;
    do{
      resull = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);
      if(resull!=1)
        handleErrors();
    }while(resull!=1);
     plaintext_len += len;

     /* Clean up */
     EVP_CIPHER_CTX_free(ctx);

     return plaintext_len;
 }


char* verificaRecursos(){

  char* status = "ruim";
  int nivel_recursos =0;
  double media[3];

  if(consultarMediaRecursos(media, 3)==1){
    printf("%lf %lf %lf\n",media[0], media[1], media[2]);
    double mediaBateria = media[0];
    double mediaMemoria = media[1];
    double mediaCpu = media[2];

    if(mediaBateria <50 && mediaBateria >= 25)
      nivel_recursos++;
    else if(mediaBateria <25)
      nivel_recursos+=3;

    if(mediaCpu > 50 && mediaCpu >= 75)
      nivel_recursos++;
    else if(mediaCpu > 75)
      nivel_recursos+=2;

    if(mediaMemoria > 50 && mediaMemoria>= 75)
      nivel_recursos++;
    else if(mediaMemoria > 75)
      nivel_recursos+=2;
  }
  //printf("%lf %lf %lf\n",media[0], media[1], media[2]);
  //Se nivel_recursos for igual a 0 é porque ocorreu algum erro
  if(nivel_recursos > 3 || nivel_recursos == 0)
    status= "ruim";

  if(status == "bom" || status == "ruim"){
    printf("Status dentro de Verifica: %s Nivel dos Recursos: %d \n", status, nivel_recursos);
    return status;
    //printf("Recursos bem distribuidos.");
  }else{
    return NULL;
  }

}

char* initCrip(char* textoclaro, int tamanhoBlocoChave,  char * criptografia){
  /*
  * Setando a Chave eo inicializador de blocos
  */
   unsigned char *chave;
   unsigned char *iv ;
   int tamanhoBloco =0;
  if( tamanhoBlocoChave == 16){

   /* Chave de 128 bits */
    chave = (unsigned char *)"ARIAEFGHIJ123456";

  }else if(tamanhoBlocoChave == 24){

    /* Chave de 192 bits */
    chave = (unsigned char *)"ARIAEFGHIJ12345678912345";


  }else if(tamanhoBlocoChave == 32){

    /* Chave de 256 bits */
    chave = (unsigned char *)"ARIAEFGHIJ1234567891234ABCDEFGHI";

    
  }

  if(strcmp(criptografia, "EVP_bf_cbc()") == 0){
    tamanhoBloco = MAX_BLOCK_64;
    iv = (unsigned char *)"ABCDEFGH";
  }else{
    tamanhoBloco = MAX_BLOCK_128;
    iv = (unsigned char *)"ARIDEFGHIJ123456";   
  }
  
  printf("\033[1;42m Tamanhos: Bloco = %d, Chave = %ld,  Bloco Inicializador = %ld \033[0m\n", tamanhoBloco, strlen(chave)*8, strlen(iv)*8 );
  /* Mensagem para ser Criptografada */
  // unsigned char *textoclaro =
  //        (unsigned char *)"Tecnologia e um termo que envolve o conhecimento tecnico e cientifico e a aplicacao deste conhecimento atraves de sua transformacaoo n uso de ferramentas. Tecnologia e um termo que envolve o conhecimento tecnico e cientifico. ua transformacaoo no uso de .";
    
    printf("TEXTO CLARO: %s\n Tamanho: %ld\n",textoclaro, strlen(textoclaro));

    unsigned char * textocifrado = (char*) malloc(tamanhoBloco*sizeof(char));
    int textocifrado_tamanho;
     /* Criptografa o textoclaro */
    textocifrado_tamanho = encrypt (textoclaro, strlen ((char *)textoclaro), chave, iv, textocifrado, criptografia, tamanhoBloco);
    
    if(textocifrado_tamanho > 0){
      textocifrado[textocifrado_tamanho] = '\0';
      imprimeTextoCifrado(textocifrado);

    }else
      handleErrors();
     /* Do something useful with the ciphertext here */
    //Imprime o texto em Formato de bloco
    // printf("Ciphertext antes de enviar:\n");
    // BIO_dump_fp (stdout, (const char *)textocifrado, textocifrado_tamanho);

    return textocifrado;

}

char* initDecrip(char* textocifrado, int tamanho, int tamanhoBlocoChave, char * criptografia){
  
  // char* status = verificaRecursos();
  // if(status != NULL)
  //   printf("%s\n", status);
  unsigned char *chave;
  unsigned char *iv ;
  int tamanhoBlocoAtual =0;
  
  if( tamanhoBlocoChave == 16){

   /* Chave de 128 bits */
    chave = (unsigned char *)"ARIAEFGHIJ123456";

  }else if(tamanhoBlocoChave == 24){

    /* Chave de 192 bits */
    chave = (unsigned char *)"ARIAEFGHIJ12345678912345";

  }else if(tamanhoBlocoChave == 32){

    /* Chave de 256 bits */
    chave = (unsigned char *)"ARIAEFGHIJ1234567891234ABCDEFGHI";
  }

  if(strcmp(criptografia, "EVP_bf_cbc()") == 0){
    tamanhoBlocoAtual = MAX_BLOCK_64;
    /*Inicializador de 64 bit IV */
    iv = (unsigned char *)"ABCDEFGH";
  }else{
    tamanhoBlocoAtual = MAX_BLOCK_128;
    /*Inicializador de 128 bit IV */
    iv = (unsigned char *)"ARIDEFGHIJ123456";
  }

  /* Buffer for the decrypted text */
  unsigned char *textoclaro = (char*) malloc(tamanhoBlocoAtual*sizeof(char));

  int textoclaro_tamanho, textocifrado_tamanho;
  printf("\033[1;42m Tamanhos: Bloco = %d, Chave = %ld,  Bloco Inicializador = %ld \033[0m\n", tamanhoBlocoAtual, strlen(chave)*8, strlen(iv)*8 );

  /* Decrypt the ciphertext */
  textoclaro_tamanho = decrypt(textocifrado, tamanhoBlocoAtual, chave, iv,
                               textoclaro, criptografia, tamanhoBlocoAtual);

  /* Add a NULL terminator.  We are expecting printable text */
  textoclaro[textoclaro_tamanho] = '\0';
  return textoclaro;
  /* Show the decrypted text */
  // printf("Decrypted text is:\n");
  // printf("%s\n", decryptedtext);
}

void imprimeTextoCifrado(char *textocifrado){
  if(textocifrado != NULL || textocifrado != " " || textocifrado[0] != '\0'){
    printf("Texto Cifrado:\n");
    BIO_dump_fp (stdout, textocifrado,  strlen ((char *)textocifrado));
  }else
    printf("Erro ao imprimir bloco!!\n");
}