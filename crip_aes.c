#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include "mongoEnv.c"

char* initCrip(char* );
char* initDecrip(char* );
int encrypt(unsigned char *, int, unsigned char *, unsigned char *, unsigned char *);
int decrypt(unsigned char *, int , unsigned char *, unsigned char *, unsigned char *);
char* verificaRecursos();
void handleErrors();

void handleErrors(void){
     ERR_print_errors_fp(stderr);
     abort();
}

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *chave,
             unsigned char *iv, unsigned char *ciphertext)
 {
     EVP_CIPHER_CTX *ctx;

     int len;

     int ciphertext_len;

     /* Create and initialise the context */
     if(!(ctx = EVP_CIPHER_CTX_new()))
         handleErrors();

     /*
      * Initialise the encryption operation.  IMPORTANT - ensure you use a chave
      * and IV size appropriate for your cipher
      * In this example we are using 256 bit AES (ie a 256 bit chave).  The
      * IV size for *most* modes is the same as the block size.  For AES this
      * is 128 bits
      *///EVP_bf_cbc(void)
     if(1 != EVP_EncryptInit_ex(ctx, EVP_bf_cbc() /*EVP_aes_256_cbc()*/, NULL, chave, iv))
         handleErrors();

     /*
      * Provide the message to be encrypted, and obtain the encrypted output.
      * EVP_EncryptUpdate can be called multiple times if necessary
      */
     if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
         handleErrors();
     ciphertext_len = len;

     /*
      * Finalise the encryption.  Further ciphertext bytes may be written at
      * this stage.
      */
     if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
         handleErrors();
     ciphertext_len += len;

     /* Clean up */
     EVP_CIPHER_CTX_free(ctx);

     return ciphertext_len;
 }


int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *chave,
             unsigned char *iv, unsigned char *plaintext)
 {
     EVP_CIPHER_CTX *ctx;

     int len;

     int plaintext_len;

     /* Create and initialise the context */
     if(!(ctx = EVP_CIPHER_CTX_new()))
         handleErrors();

     /*
      * Initialise the decryption operation.  IMPORTANT - ensure you use a chave
      * and IV size appropriate for your cipher
      * In this example we are using 256 bit AES (ie a 256 bit chave).  The
      * IV size for *most* modes is the same as the block size.  For AES this
      * is 128 bits EVP_aes_256_cbc()
      */
     if(1 != EVP_DecryptInit_ex(ctx,EVP_bf_cbc() , NULL, chave, iv))
         handleErrors();

     /*
      * Provide the message to be decrypted, and obtain the plaintext output.
      * EVP_DecryptUpdate can be called multiple times if necessary.
      */
     if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
         handleErrors();
     plaintext_len = len;

     /*
      * Finalise the decryption.  Further plaintext bytes may be written at
      * this stage.
      */
     printf("Iniciou o Final Decrypted\n");
     if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len))
         handleErrors();
     plaintext_len += len;

     /* Clean up */
     EVP_CIPHER_CTX_free(ctx);

     return plaintext_len;
 }


char* verificaRecursos(){

  char* status = "bom";
  int nivel_recursos =0;
  double media[3];

  if(consultarMediaRecursos(media, 3)){
    double mediaBateria = media[0];
    double mediaMemoria = media[1];
    double mediaCpu = media[2];

    if(mediaBateria <50 && mediaBateria >= 25)
      nivel_recursos++;
    else if(mediaBateria <25)
      nivel_recursos+=2;

    if(mediaCpu > 50 && mediaCpu >= 75)
      nivel_recursos++;
    else if(mediaCpu > 75)
      nivel_recursos+=2;

    if(mediaMemoria > 50 && mediaMemoria>= 75)
      nivel_recursos++;
    else if(mediaMemoria > 75)
      nivel_recursos+=2;
  }
  //Se nivel_recursos for igual a 0 é porque ocorreu algum erro
  if(nivel_recursos > 3 || nivel_recursos == 0)
    strcpy(status, "ruim");

  if(status == "bom" || status == "ruim"){
    printf("Status dentro de Verifica: %s\n Nivel dos Recursos: %d \n", status, nivel_recursos);
    return status;
    //printf("Recursos bem distribuidos.");
  }else{
    return NULL;
  }

}

char* initCrip(char* textoclaro){
  /*
  * Setandos a Chave eo inicializador de blocos
  */

  /* Chave de 256 bits */
  unsigned char *chave256 = (unsigned char *)"ABCDEFGHIJ0123456789ABCDEFGHIJ01";
  unsigned char *chave192 = (unsigned char *)"ABCDEFGHIJ0123456789ABCDEFGHIJ01";
  unsigned char *chave128 = (unsigned char *)"ABCDEFGHIJ0123456789ABCDEFGHIJ01";
  unsigned char *chave192 = (unsigned char *)"ABCDEFGHIJ0123456789ABCDEFGHIJ01";
  /*Inicializador de 128 bit IV */
  unsigned char *iv = (unsigned char *)"SEGREDODAVIDAEVC";

  /* Mensagem para ser Criptografada */
  // unsigned char *textoclaro =
  //        (unsigned char *)"Tecnologia e um termo que envolve o conhecimento tecnico e cientifico e a aplicacao deste conhecimento atraves de sua transformacaoo n uso de ferramentas. Tecnologia e um termo que envolve o conhecimento tecnico e cientifico. ua transformacaoo no uso de .";
    
    printf("TEXTO CLARO: %s\n Tamanho: %ld\n",textoclaro, strlen(textoclaro));

    unsigned char * textocifrado;
    int textocifrado_tamanho;
     /* Criptografa o textoclaro */
    textocifrado_tamanho = encrypt (textoclaro, strlen ((char *)textoclaro), chave, iv,
                               textocifrado);

     /* Do something useful with the ciphertext here */
    //Imprime o texto em Formato de bloco
    //printf("Ciphertext is:\n");
    //BIO_dump_fp (stdout, (const char *)textocifrado, textocifrado_tamanho);

    return textocifrado;

}

char* initDecrip(char* textocifrado){
  char* status = verificaRecursos();
  if(status != NULL)
    printf("%s\n", status);
  /* A 256 bit chave */
  unsigned char *chave = (unsigned char *)"ABCDEFGHIJ0123456789ABCDEFGHIJ01";
  /* A 128 bit IV */
  unsigned char *iv = (unsigned char *)"SEGREDODAVIDAEVC";
  /* Buffer for the decrypted text */
  unsigned char *textoclaro;

  int textoclaro_tamanho, textocifrado_tamanho;

  /* Decrypt the ciphertext */
  textoclaro_tamanho = decrypt(textocifrado, strlen ((char *)textocifrado), chave, iv,
                               textoclaro);

  /* Add a NULL terminator.  We are expecting printable text */
  textoclaro[textoclaro_tamanho] = '\0';
  return textoclaro;
  /* Show the decrypted text */
  // printf("Decrypted text is:\n");
  // printf("%s\n", decryptedtext);
}



// int main (void)
//  {  
//     char* status = verificaRecursos();
//       if(status != NULL)
//     printf("%s\n", status);
//      /*
//       * Set up the chave and iv.  Do I need to say to not hard code these in a
//       * real application?  :-)
//       */

//      /* A 256 bit chave */
//      unsigned char *chave = (unsigned char *)"ABCDEFGHIJ0123456789ABCDEFGHIJ01";

//      /* A 128 bit IV */
//      unsigned char *iv = (unsigned char *)"SEGREDODAVIDAEVC";

//      /* Message to be encrypted */
//      unsigned char *plaintext =
//          (unsigned char *)"Tecnologia e um termo que envolve o conhecimento tecnico e cientifico e a aplicacao deste conhecimento atraves de sua transformacaoo n uso de ferramentas. Tecnologia e um termo que envolve o conhecimento tecnico e cientifico. ua transformacaoo no uso de .";
//       printf("TEXTO CLARO: %s\n Tamanho: %ld\n",plaintext, strlen(plaintext));

     
//       * Buffer for ciphertext.  Ensure the buffer is long enough for the
//       * ciphertext which may be longer than the plaintext, depending on the
//       * algorithm and mode.
      
//      unsigned char ciphertext[256];

//      /* Buffer for the decrypted text */
//      unsigned char decryptedtext[256];

//      int decryptedtext_len, ciphertext_len;

//      /* Encrypt the plaintext */
//      ciphertext_len = encrypt (plaintext, strlen ((char *)plaintext), chave, iv,
//                                ciphertext);

//      /* Do something useful with the ciphertext here */
//      printf("Ciphertext is:\n");
//      BIO_dump_fp (stdout, (const char *)ciphertext, ciphertext_len);

//      /* Decrypt the ciphertext */
//      decryptedtext_len = decrypt(ciphertext, ciphertext_len, chave, iv,
//                                  decryptedtext);

//      /* Add a NULL terminator.  We are expecting printable text */
//      decryptedtext[decryptedtext_len] = '\0';

//      /* Show the decrypted text */
//      printf("Decrypted text is:\n");
//      printf("%s\n", decryptedtext);


//      return 0;
//  }