#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>

#define MAX_MSGB 1024

typedef struct _parametros_cifra{
    unsigned char *chave;
    unsigned char *iv;
    const EVP_CIPHER *tipo_cifra;
}params_cifra;

int encrypt(unsigned char *, int , params_cifra *, unsigned char *);
int decrypt(unsigned char *, int , params_cifra *, unsigned char *);
void handleErrors(void);


void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    // abort();
}



int encrypt(unsigned char *plaintext, int plaintext_len, params_cifra *params, unsigned char *ciphertext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;

    /* Criando o contexto da criptografia*/
    if(!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    /*
    Iniciando o processo criptográfico onde temos 
        EVP_bf_cbc()
        EVP_aes_128_cbc()
        EVP_aes_192_cbc()
        EVP_aes_256_cbc()

     */
    if(1 != EVP_EncryptInit_ex(ctx, params->tipo_cifra, NULL, params->chave, params->iv))
        handleErrors();

    /*
     * Provide the message to be encrypted, and obtain the encrypted output.
     * EVP_EncryptUpdate can be called multiple times if necessary
     */
    memset(ciphertext, '\0',MAX_MSGB);
    if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        handleErrors();
    ciphertext_len = len;

    /*
     * Finalise the encryption. Further ciphertext bytes may be written at
     * this stage.
     */
    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        handleErrors();
    ciphertext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}


int decrypt(unsigned char *ciphertext, int ciphertext_len, params_cifra *params, unsigned char *plaintext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int plaintext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();
    // printf("Contexto criado\n");
    /*
     * Initialise the decryption operation. IMPORTANT - ensure you use a key
     * and IV size appropriate for your cipher
     * In this example we are using 256 bit AES (i.e. a 256 bit key). The
     * IV size for *most* modes is the same as the block size. For AES this
     * is 128 bits
     */
    if(1 != EVP_DecryptInit_ex(ctx, params->tipo_cifra, NULL, params->chave, params->iv))
        handleErrors();

    // printf("Inicio da decrip\n");
    /*
     * Provide the message to be decrypted, and obtain the plaintext output.
     * EVP_DecryptUpdate can be called multiple times if necessary.
     */
    memset(plaintext, '\0',MAX_MSGB);
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
        handleErrors();
    plaintext_len = len;

    // printf("Quase lá EVP_DecryptUpdate\n");
    /*
     * Finalise the decryption. Further plaintext bytes may be written at
     * this stage.
     */
    if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)){
        handleErrors();
        return -1;
    }
    plaintext_len += len;
     // printf("Finalizando EVP_DecryptFinal_ex\n");

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);
    // printf("Finalizado e return tamanho do texto\n");
    return plaintext_len;
}
