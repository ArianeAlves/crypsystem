#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "evpTestes.c"
// #include "analiseRec.c"

#define MAX_MSG 1024
#define MAX_MSG128 16
#define MAX_SIZE_chave128 16
#define MAX_SIZE_chave192 24
#define MAX_SIZE_chave256 32

// typedef struct _parametros_cifra{
//     unsigned char *chave;
//     unsigned char *iv;
//     const EVP_CIPHER *tipo_cifra;
// }params_cifra;

params_cifra *params;

int decrip(unsigned char *, unsigned char *, int);
// void handleErrors(void);
void trocaCrip(char *crip){
	params_cifra *parametros = (params_cifra *)malloc(sizeof(params_cifra));
	params_cifra *varlimpar = params;
	if(strcmp(crip, "##AES256##") == 0){
		/* A 256 bit chave */
    	parametros->chave = (unsigned char *)"01234567890123456789012345678901";
    	parametros->iv = (unsigned char *)"UTF3456789012345";
    	parametros->tipo_cifra = EVP_aes_256_cbc();

	}else if ( strcmp(crip, "##AES192##") == 0){
		/* A 192 bit chave */
		parametros->chave = (unsigned char *)"KKKKKKKK888985888582525T";
    	parametros->iv = (unsigned char *)"BRA3456789012345";
    	parametros->tipo_cifra = EVP_aes_192_cbc();

	}else if (strcmp(crip, "##AES128##") == 0){
    	/* A 128 bit chave */
		parametros->chave = (unsigned char *)"ARIAEFGHIJ123456";
    	parametros->iv = (unsigned char *)"VOA3456789012345";
    	parametros->tipo_cifra = EVP_aes_128_cbc();
	}else if (strcmp(crip, "##BF128##") == 0){
    	/* A 128 bit chave */
		parametros->chave = (unsigned char *)"PARAEFGHIJ123456";
    	parametros->iv = (unsigned char *)"ARIANE25";
    	parametros->tipo_cifra = EVP_bf_cbc();
	} else if (strcmp(crip, "##BF192##") == 0){
    	/* A 192 bit chave */
		parametros->chave = (unsigned char *)"KKKKKKKK888985888582525T";
    	parametros->iv = (unsigned char *)"BABA4538";
    	parametros->tipo_cifra = EVP_bf_cbc();
	} else if (strcmp(crip, "##BF256##") == 0){
    	/* A 256 bit chave */
		parametros->chave = (unsigned char *)"01234567890123456789012345678901";
    	parametros->iv = (unsigned char *)"TIO9K2K2";
    	parametros->tipo_cifra = EVP_bf_cbc();
	}

	if(!params){
		params = parametros;
		free(varlimpar);
	}else{
		params = parametros;
	}
	
	
	printf("Troca Realizada\n");
}

// void handleErrors(void)
// {
//     ERR_print_errors_fp(stderr);
//     abort();
// }

int decrip(unsigned char *textocifrado, unsigned char *textoclaro, int tamanho){

    printf("Texto Cifrado :\n");
    BIO_dump_fp (stdout, (const char *)textocifrado, tamanho);

    int textoclaro_tamanho;

    /* Descifrando textocifrado */
    textoclaro_tamanho = decrypt(textocifrado, tamanho, params,
                                textoclaro);

    textoclaro[textoclaro_tamanho] = '\0';

    /* Mostrando o texto claro */
    printf("Texto Claro:\n");
    printf("%s\n", textoclaro);

    return 0;

}

int crip (unsigned char *textoclaro, unsigned char *textocifrado){

 
    int  textocifrado_tamanho;

    /* Criptografar o textoclaro */
    textocifrado_tamanho = encrypt(textoclaro, strlen ((char *)textoclaro), params,
                              textocifrado);

    /* Imprimindo texto cifrado */
    printf("Texto Cifrado :\n");
    BIO_dump_fp (stdout, (const char *)textocifrado, textocifrado_tamanho);

    return 0;
}


/* 
 - Espera cliente conectar
 - Cria nova thread para o cliente
 - Envia mensagem de boas vindas para o cliente
 - Espera mensagem do cliente
*/
void *tratador_conexao(void *);



void *tratador_conexao(void *conexao)
{
	int sock = *(int *)conexao;
	int tamanho;
	char recebida[MAX_MSG];
	char *mensagem;
	char chaveAtual[MAX_MSG];
	char textocifrado[MAX_MSG];
	int deint = 0;
	//Enviando mensagem para o cliente
	mensagem = "Serv> Bem vindo.";
	write(sock, mensagem, strlen(mensagem));
	//Inicializando o tipo de criptografia a ser utilizada
	trocaCrip("##AES256##");
	fflush(stdin);
	while ((tamanho = recv(sock, textocifrado, MAX_MSG, 0)) > 0)
	{	
		
		printf("Tamanho recebido %d \n", tamanho);
		while(decrip(textocifrado, recebida, strlen(textocifrado))<0){
			fflush(stdout);
			write(sock, "##ERROR##", strlen("##ERROR##"));
		}
		recebida[strlen(textocifrado)] = '\0';

		if(strcmp(recebida, "##AES256##") == 0 
			|| strcmp(recebida, "##AES192##") == 0 
			|| strcmp(recebida, "##AES128##") == 0
			|| strcmp(recebida, "##BF256##") == 0
			|| strcmp(recebida, "##BF192##") == 0
			|| strcmp(recebida, "##BF128##") == 0 )
		{
			
			printf("\033[1;42m Iniciando a troca de Criptografia \033[0m\n");
			trocaCrip(recebida);
			fflush(stdin);
			write(sock, "##TROCAOK##", strlen("##TROCAOK##"));

		}else{
			fflush(stdout);
			write(sock, "##OK##", strlen("##OK##"));

		}
		// printf("%s\n", recebida);
		// write(sock, "##TROCAOK##", strlen("##TROCAOK##"));
		// limpa as variaveis
		memset(textocifrado, 0,MAX_MSG);
		memset(recebida, 0, MAX_MSG);
		fflush(stdin);
		// free(recebida);
		// free(textocifrado);
	
	}

	if (tamanho == 0)
	{
		puts("Cliente desconectou.\n");
		fflush(stdout);
	}
	else if (tamanho == -1)
	{
		perror("erro no recebimento: \n Cliente Desconetado!\n ");
	}

	//Liberando o ponteiro
	free(conexao);

	return 0;
}

int main(void)
{
	//variaveis
	int socket_desc, conexao, c, *nova_conexao;
	struct sockaddr_in server, client;
	char *mensagem;
	char resposta[MAX_MSG];
	int nbytes, count;

	// para pegar o IP e porta do cliente
	char *client_ip;
	int client_port;

	/*********************************************************/
	//Criando um socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1)
	{
		printf("Não foi possivel criar o socket\n");
		return -1;
	}

	int reuso = 1;
	if (setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuso, sizeof(reuso)) < 0)
	{
		perror("Não foi possivel reusar endereço");
		return -1;
	}
#ifdef SO_REUSEPORT
	if (setsockopt(socket_desc, SOL_SOCKET, SO_REUSEPORT, (const char *)&reuso, sizeof(reuso)) < 0)
	{
		perror("Não foi possível reusar porta");
		return -1;
	}
#endif

	//Preparando a struct do socket
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY; // Obtem IP do S.O.
	server.sin_port = htons(1234);

	//Associando o socket a porta e endereco
	if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
	{
		puts("Erro ao fazer bind\n");
	}
	puts("Bind efetuado com sucesso\n");

	// Ouvindo por conexoes
	listen(socket_desc, 3);

	/*********************************************************/
	//Aceitando e tratando conexoes
	puts("Aguardando por conexoes...");
	c = sizeof(struct sockaddr_in);
	// Fica esperando por conexoes
	while ((conexao = accept(socket_desc, (struct sockaddr *)&client, (socklen_t *)&c)))
	{
		if (conexao < 0)
		{
			perror("Erro ao receber conexao\n");
			return -1;
		}
		// pegando IP e porta do cliente
		client_ip = inet_ntoa(client.sin_addr);
		client_port = ntohs(client.sin_port);
		printf("Cliente conectou: %s : [ %d ]\n", client_ip, client_port);

		/**** Criando thread para tratar da comunicacao ******/
		pthread_t processo;
		nova_conexao = malloc(1);
		*nova_conexao = conexao;

		if (pthread_create(&processo, NULL, tratador_conexao, (void *)nova_conexao) < 0)
		{
			perror("nao foi possivel criar thread: ");
			return -1;
		}

	} //fim do while

	/*********************************************************/

	close(socket_desc);
	shutdown(socket_desc, 2);

	printf("Servidor finalizado...\n");
	return 0;
}