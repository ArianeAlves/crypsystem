#include <bson.h>
#include <mongoc.h> 
#include <stdio.h> 
#include <time.h>
#include <string.h>


#define MAX_MSG 1024
#define MAX_MSG128 16
#define MAX_SIZE_chave128 16
#define MAX_SIZE_chave192 24
#define MAX_SIZE_chave256 32

int inicioInsert(char *);
struct tm dataNow();
bson_t* formataJson(char *);
int insertMongo(bson_t *);
int consultarMediaRecursos();



int inicioInsert(char *texto){  
   bson_error_t error ;
   bson_t      * bson ;
   int try = 0;
   //texto = "{ \"memoria\" : 89.4 , \"bateria\": 67.0 , \"cpu\" : 69.5 }";
    while(try < 2){
      bson = formataJson(texto);

      if ( ! bson ) {
        fprintf ( stderr , "Erro ao transformar em Json%s \n " , error.message );
        try++;
        // return 1/*EXIT_FAILURE*/ ;
      }else{
        
        if (insertMongo(bson) == 1){
           printf("==OK INSERT==\n");
           return 0;
        }
        try++;
     
      }
      
    }
   return 1;
}

struct tm dataNow(){
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  // tm.tm_year += 1900;
  // tm.tm_mon += 1;
  tm.tm_hour-=3;
  //printf("now: %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1,tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
  return tm;
}

bson_t* formataJson(char *texto){
   bson_error_t error ;
   bson_t      * bson ;
   //char *string;
   printf("UTF-8  %s\n", ( const uint8_t * ) texto);
   bson = bson_new_from_json (( const uint8_t * ) texto ,-1 /*strlen(texto)*/, & error );
   // string = bson_as_canonical_extended_json (bson, NULL);
   // printf ("Oiii %s\n", string);
   // bson_free (string);
   return bson;
}


int insertMongo(bson_t *doc){
    mongoc_client_t * client ;
    mongoc_collection_t * collection ;
    bson_error_t error ;
    bson_oid_t oid ;
    struct tm tm;

    mongoc_init ();
    // printf("insert Inicializou\n");
    client = mongoc_client_new ( "mongodb://localhost:27017/" );
    collection = mongoc_client_get_collection ( client , "dbRecursosCliente" , "trocaAutomaticaChaves1" );
    
    tm = dataNow();
    bson_append_date_time (doc, "data", -1, mktime (&tm) * 1000);

    if ( ! mongoc_collection_insert (
           collection ,MONGOC_INSERT_NONE, doc , NULL, & error )) {
         printf ("%s \n " , error.message );
         return 0;
    }else{

       bson_destroy ( doc );
       mongoc_collection_destroy ( collection );
       mongoc_client_destroy ( client );
       mongoc_cleanup ();
       return 1;
    }
   

}

int consultarMediaRecursos(double dado[], int tamanho){
  mongoc_client_t *client;
  mongoc_collection_t *collection;
  mongoc_cursor_t *cursor;
  bson_error_t error;
  const bson_t *doc;
  bson_t *query;
  bson_t *query1;
  bson_t *query2;
  time_t segundos;
  time(&segundos);
  struct tm tm; 
  char *str;
  char *string;
  mongoc_init ();
  long int horas =0;
  
  client = mongoc_client_new ( "mongodb://127.0.0.1:27017" );
  collection = mongoc_client_get_collection ( client , "dbRecursosCliente" , "trocaAutomaticaChaves1" );
  tm = dataNow();
  //bson_append_date_time (query1, "data", -1, mktime (&tm) * 1000);
  //bson_append_date_time (query1, "date", -1, mktime(&tm)*1000);
  //bson_append_date_time (query2, "$date", -1, (segundos-7200)*1000);
  //bson_append_timestamp (b, key, (int) strlen (key), val, inc)
  //str = bson_as_canonical_extended_json (query1, NULL);
  //printf("%s\n", str);

  //Realiza a média dos dados dos últimops 5 minutos
  horas = (mktime(&tm));
  printf("Timer da media: %ld\n", (horas-300)*1000);
  query = BCON_NEW("pipeline", "[", 
    "{","$match", "{","data", 
      "{","$gte", BCON_DATE_TIME((horas-300)*1000), 
      "$lt", BCON_DATE_TIME((horas)*1000), "}",
   "}", "}",
   "{" ,"$group", 
      "{", "_id" , "mediasRecursos", 
         "mediamemoria", "{" ,"$avg", "$memoria","}", 
        "mediabateria", "{", "$avg","$bateria","}",         
         "mediacpu",  "{" , "$avg" , "$cpu","}",
      "}", 
   "}",
   "]");

  //string = bson_as_canonical_extended_json (query, NULL);
  //printf("%s\n", string);
  
  int i=0;
  cursor = mongoc_collection_aggregate (collection, MONGOC_QUERY_NONE, query, NULL, NULL);
  while (mongoc_cursor_next (cursor, &doc)==1){
    str =  bson_array_as_json(doc, NULL);
    // printf("Verificação %s\n", str);
    char delim[] = ",";
    char *ptr = strtok(str, "[ \"mediasRecursos\", ");
    
    while (ptr != NULL && i < tamanho)
    {
      dado[i] = atof(ptr);
      printf("%lf\n", dado[i++]);
      //printf(" %d '%s'\n",i++, ptr);
      ptr = strtok(NULL, ", ]");
      
    }
     //bson_as_canonical_extended_json tranforma em json
      
    bson_free (str);
  }

  if (mongoc_cursor_error (cursor, &error)) {
    fprintf (stderr, "Cursor Failure: %s\n", error.message);
  }

  bson_destroy (query);
  mongoc_cursor_destroy (cursor);
  mongoc_collection_destroy (collection);
  mongoc_client_destroy (client);
  mongoc_cleanup ();

  if(i>=tamanho){
    return 1;
  }
  return 0;
}