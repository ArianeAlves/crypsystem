#include <bson.h>
#include <mongoc.h> 
#include <stdio.h> 
#include <time.h>
#include <string.h>
// #include "bancoMongo.c"

#define MAX_MSG 1024
#define MAX_BLOCK_64 8
#define MAX_BLOCK_128 16
#define MAX_BLOCK_192 24
#define MAX_BLOCK_256 32

char* arquivo();
char* verificaRecursos();
void tabelaCriptografias(int , int , char *, int *);
int timerUltimaTroca(time_t , time_t *);
void troca_crip(void *, char* , int );


//Função que realiza a troca de criptografia nas variaveis
void tabelaCriptografias(int indexCrip, int indexBloco, char *newCriptografia, int *tamanhoBlocoAtual){
  char tableCrip[4][20] = {"EVP_bf_cbc()", "EVP_aes_128_cbc()", "EVP_aes_192_cbc()", "EVP_aes_256_cbc()"};
  int tamanhoBlocos[3] = {MAX_BLOCK_128, MAX_BLOCK_192, MAX_BLOCK_256};
  strcpy(newCriptografia, tableCrip[indexCrip]);
  //newCriptografia[strlen(newCriptografia)]= '\0';
  *tamanhoBlocoAtual = tamanhoBlocos[indexBloco];
  printf("Tamanho do Bloco atual:%d\n Criptografia: %s \n", *tamanhoBlocoAtual, newCriptografia);
  //tamanhoBlocoAtual[strlen(tamanhoBlocoAtual)] = '\0';
}

int timerUltimaTroca(time_t troca, time_t *fim){
  time_t now, gasto;
  time(&now);

  gasto = now - troca;
  printf("Timer: %ld/ inicio: %ld/ agora: %ld\n", gasto, troca, now);
  //Verificação a cada 1 min = 60 segundos
  if(gasto > 60){
      //time(fim);
      printf("Inicio da Troca\n");
      return 1;
  }
    return 0;
}

void troca_crip(void *conexao, char* newCriptografia, int tamanhoBlocoAtual){
  int sock = *(int*)conexao;
  char concatena[256];
  char *recebida;
  char *buffer;
  int tamanho;
  // strcat(concatena, "Iniciar processo de troca: ");
  // strcat(concatena, newCriptografia);
  // sprintf(buffer, "%d", tamanhoBlocoAtual);
  // strcat(concatena, buffer);
  sprintf( concatena, "Iniciar processo de troca: %s %d", newCriptografia, tamanhoBlocoAtual);
  concatena[strlen(concatena)] = '\0';
  //printf("Teste1: %s\n", concatena);
  write(sock,concatena, strlen(concatena));
  while ((tamanho = recv(sock, recebida, tamanhoBlocoAtual, 0)) > 0);

  if (recebida == "Troca Finalizada")
  {
    printf("Troca realizada com Sucesso\n");
  }
  strcpy(concatena, "\0");
}

//Coleta de informações de cpu, memoria e bateria de um cliente
char* arquivo(){
  
  system("echo {\"memoria\" : `smem -m -p - t -c rss | tail -1| sed '1s/%/ /'` , > teste.dat");
  system("upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep 'percentage' | sed 's/ //g' | sed 's/[ ]\\+/ /g' | sed '1s/percentage/\"bateria\" /'| sed '1s/%/ ,/' >> teste.dat ");
  system("NUMCPUS=`grep ^proc /proc/cpuinfo | wc -l`; FIRST=`cat /proc/stat | awk '/^cpu / {print $5}'`; sleep 1; SECOND=`cat /proc/stat | awk '/^cpu / {print $5}'`; USED=`echo 2 k 100 $SECOND $FIRST - $NUMCPUS / - p | dc`; echo \"cpu\": ${USED}} >> teste.dat");
  system("echo `cat teste.dat | tr '\n' ' ' | sed '1s/memoria/\"memoria\" /' | sed '1s/cpu/\"cpu\" /'` > teste.dat");
  FILE *arq;
  char *mensagem = (char *) malloc(MAX_MSG * sizeof(char));
  int result;
  arq = fopen("teste.dat", "rt");
  if (arq == NULL)  // Se houve erro na abertura
    {
       printf("Problemas na abertura do arquivo\n");
       return;
    }

  result = fread (&mensagem[0], sizeof(char), MAX_MSG, arq);

  fclose(arq);

  return mensagem;
}



// Verificamos 3 tipos de recursos, por isso  
char* verificaRecursos(){

  char* status = "bom";
  int nivel_recursos =0;
  double media[3];

  if(consultarMediaRecursos(media, 3)==1){
    printf("%lf %lf %lf\n",media[0], media[1], media[2]);
    double mediaBateria = media[0];
    double mediaMemoria = media[1];
    double mediaCpu = media[2];

    if(mediaBateria <50 && mediaBateria >= 25)
      nivel_recursos++;
    else if(mediaBateria <25)
      nivel_recursos+=3;

    if(mediaCpu > 50 && mediaCpu >= 75)
      nivel_recursos++;
    else if(mediaCpu > 75)
      nivel_recursos+=2;

    if(mediaMemoria > 50 && mediaMemoria>= 75)
      nivel_recursos++;
    else if(mediaMemoria > 75)
      nivel_recursos+=2;
  }
  //printf("%lf %lf %lf\n",media[0], media[1], media[2]);
  //Se nivel_recursos for igual a 0 é porque ocorreu algum erro
  if(nivel_recursos > 3 || nivel_recursos == 0)
    status= "ruim";

  if(status == "bom" || status == "ruim"){
    printf("Status dentro de Verifica: %s Nivel dos Recursos: %d \n", status, nivel_recursos);
    return status;
    //printf("Recursos bem distribuidos.");
  }else{
    return NULL;
  }

}