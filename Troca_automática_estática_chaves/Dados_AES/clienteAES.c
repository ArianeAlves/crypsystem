#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include "evpTestes.c"
#include "analiseRec.c"
#include "bancoMongo.c"
#include <sys/sysinfo.h>
#include <linux/kernel.h>
// #include "/usr/include/proc/readproc.h"
#include "memoryusade.c"

#define MAX_MSG 1024
#define MAX_MSG128 16
#define MAX_SIZE_chave128 16
#define MAX_SIZE_chave192 24
#define MAX_SIZE_chave256 32

void* trataConversa(void *);
void inserindoDados(int *);
void trocaCrip(char *);
void arquivoTXT(int *);

typedef struct {
	int socket_desc, set;
}thread_arg;

// int timerUltimaTroca(time_t troca, time_t *fim){
// 	time_t now, gasto;
// 	time(&now);

// 	gasto = now - troca;
// 	printf("Timer: %ld/ inicio: %ld/ agora: %ld\n", gasto, troca, now);
// 	//Verificação a cada 1 min = 60 segundos
// 	if(gasto > 60){
// 	    //time(fim);
// 	    printf("Inicio da Troca\n");
// 	    return 1;
// 	}
// 	  return 0;
// }

// typedef struct _parametros_cifra{
//     unsigned char *chave;
//     unsigned char *iv;
//     const EVP_CIPHER *tipo_cifra;
// }params_cifra;

params_cifra *params ;
void trocaCrip(char *crip){
	params_cifra *parametros = (params_cifra *)malloc(sizeof(params_cifra));
	params_cifra *varlimpar = params;
	if(strcmp(crip, "##AES256##") == 0){
		/* A 256 bit chave */
    	parametros->chave = (unsigned char *)"01234567890123456789012345678901";
    	parametros->iv = (unsigned char *)"UTF3456789012345";
    	parametros->tipo_cifra = EVP_aes_256_cbc();

	}else if ( strcmp(crip, "##AES192##") == 0){
		/* A 192 bit chave */
		parametros->chave = (unsigned char *)"KKKKKKKK888985888582525T";
    	parametros->iv = (unsigned char *)"BRA3456789012345";
    	parametros->tipo_cifra = EVP_aes_192_cbc();

	}else if (strcmp(crip, "##AES128##") == 0){
    	/* A 128 bit chave */
		parametros->chave = (unsigned char *)"ARIAEFGHIJ123456";
    	parametros->iv = (unsigned char *)"VOA3456789012345";
    	parametros->tipo_cifra = EVP_aes_128_cbc();
	}else if (strcmp(crip, "##BF128##") == 0){
    	/* A 128 bit chave */
		parametros->chave = (unsigned char *)"PARAEFGHIJ123456";
    	parametros->iv = (unsigned char *)"ARIANE25";
    	parametros->tipo_cifra = EVP_bf_cbc();
	} else if (strcmp(crip, "##BF192##") == 0){
    	/* A 192 bit chave */
		parametros->chave = (unsigned char *)"KKKKKKKK888985888582525T";
    	parametros->iv = (unsigned char *)"BABA4538";
    	parametros->tipo_cifra = EVP_bf_cbc();
	} else if (strcmp(crip, "##BF256##") == 0){
    	/* A 256 bit chave */
		parametros->chave = (unsigned char *)"01234567890123456789012345678901";
    	parametros->iv = (unsigned char *)"TIO9K2K2";
    	parametros->tipo_cifra = EVP_bf_cbc();
	} 

	if(!params){
		params = parametros;
		free(varlimpar);
	}else{
		params = parametros;
	}
	
	
	printf("Troca Realizada\n");
}


int decrip(unsigned char *textocifrado, unsigned char *textoclaro, int tamanho){


    int textoclaro_tamanho;

    /* Descifrando textocifrado */
    textoclaro_tamanho = decrypt(textocifrado, tamanho, params,
                                textoclaro);

    textoclaro[textoclaro_tamanho] = '\0';

    /* Mostrando o texto claro */
    printf("Texto Claro:\n");
    printf("%s\n", textoclaro);

    return 0;

}

int crip (unsigned char *textoclaro, unsigned char *textocifrado){

    
    int  textocifrado_tamanho;

    /* Criptografar o textoclaro */
    textocifrado_tamanho = encrypt(textoclaro, strlen ((char *)textoclaro), params,
                              textocifrado);

    /* Imprimindo texto cifrado */
    printf("Texto Cifrado :\n");
    BIO_dump_fp (stdout, (const char *)textocifrado, textocifrado_tamanho);

    return 0;
}

// Função para salvar os dados em um arquivo
// void arquivoTXT(int *ref){
// 	int count = 0;
// 	struct sysinfo si;
//  	const double megabyte = 1024 * 1024;

//  	struct proc_t usage;
  
// 	system("figlet 'Teste Blowfish Criptogrando 32 blocos de 128bits = 4096 bits' >> RESULTADOS_allchaves.txt");
// 	system("date '+%A-feira dia %d de %B de %Y'  >> RESULTADOS_allchaves.txt");
// 	system("date '+Horário Início %T' >> RESULTADOS_allchaves.txt" );
		
// 	while(*ref){
// 		sysinfo (&si);
// 		printf ("total RAM   : %5.1f MB\n", si.totalram / megabyte);
//  		printf ("free RAM   : %5.1f MB\n", (si.totalram - si.freeram) / megabyte);
//  		printf ("Memoria utilizada RAM   : %5.1f MB\n", si.bufferram / megabyte);

//  		look_up_our_self(&usage);
//   		printf("usage: %lu\n", usage.vsize);

// 		system("echo {\"memoria\" : `smem -m -p - t -c rss | tail -1| sed '1s/%/ /'` , >> RESULTADOS_allchaves.txt");
// 		system("upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep 'percentage' | sed 's/ //g' | sed 's/[ ]\\+/ /g' | sed '1s/percentage/\"bateria\" /' | sed '1s/:/: /' | sed '1s/%/ ,/' >> RESULTADOS_allchaves.txt ");
// 		system("NUMCPUS=`grep ^proc /proc/cpuinfo | wc -l`; FIRST=`cat /proc/stat | awk '/^cpu / {print $5}'`; sleep 1; SECOND=`cat /proc/stat | awk '/^cpu / {print $5}'`; USED=`echo 2 k 100 $SECOND $FIRST - $NUMCPUS / - p | dc`; echo \"cpu\" : ${USED}} , >> RESULTADOS_allchaves.txt");
		
// 		// system("echo `cat Dados_AES/teste.dat | tr '\n' ' ' | sed '1s/memoria/\"memoria\" /' | sed '1s/cpu/\"cpu\" /'` > Dados_AES/teste.dat");
// 		// FILE *arq;
// 		// char *mensagem = (char *) malloc(MAX_MSG * sizeof(char));
// 		// int result;
// 		// arq = fopen("Dados_AES/teste.dat", "rt");
// 		// if (arq == NULL)  // Se houve erro na abertura
// 		//   {
// 		//      printf("Problemas na abertura do arquivo\n");
// 		//      return;
// 		//   }

// 		// result = fread (&mensagem[0], sizeof(char), MAX_MSG, arq);
// 		// printf("Dados Cap : %s\n", mensagem);
// 		// fclose(arq);
// 	}

// 	system("date '+Horário Fim %T' >> RESULTADOS_allchaves.txt");
// 	system("date '+%A-feira dia %d de %B de %Y'  >> RESULTADOS_allchaves.txt");
// 	// system("ETIME=`expr $end - $start`");
// 	// system("echo -n Tempo de exercução: ${ETIME} >> Dados_BF/BF_256.txt");
// 	printf("Cliente finalizado com sucesso!\n");

// 	*ref = -1;
// 	pthread_exit(NULL);
// }

// void inserindoDados(int *ref){
// 	char * texto ;
// 	int insertOK;

// 	while(*ref){
// 		fflush(stdin);
// 		texto = arquivo();
// 		texto[strlen(texto)] = '\0';
// 		insertOK =  inicioInsert(texto);
// 		if( insertOK == 1)
// 			printf("=====Erro no Insert======\n");
// 	}

// 	*ref = -1;
// 	pthread_exit(NULL);

// }



int main(int argc , char *argv[]){
	// variaveis
	int socket_desc;
	struct sockaddr_in servidor;
	int conv;

	/*****************************************/
	/* Criando um socket */
	// AF_INET = ARPA INTERNET PROTOCOLS
	// SOCK_STREAM = TCP
	// 0 = protocolo IP
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
     
	if (socket_desc == -1){
		printf("Nao foi possivel criar socket\n");
		return -1;	
	}

	/* Informacoes para conectar no servidor */
	// IP do servidor
	// familia ARPANET
	// Porta - hton = host to network short (2bytes)
	servidor.sin_addr.s_addr = inet_addr("127.0.0.1");
	servidor.sin_family = AF_INET;
	servidor.sin_port = htons( 1234 );

	//Conectando no servidor remoto
	if (connect(socket_desc , (struct sockaddr *)&servidor , sizeof(servidor)) < 0){
		printf("Nao foi possivel conectar\n");
		return -1;
	}
	printf("Conectado no servidor\n");

	pthread_t infosCliente[2];
	thread_arg *args = (thread_arg *)malloc(sizeof(thread_arg));
	printf("Socket %d\n",socket_desc );
	args->socket_desc = socket_desc;
	printf("Socket depois da passgem: %d\n", args->socket_desc );
	args->set= 1;
	/**** Criando thread para coleta de dados durante a realização das tarefas do cliente******/
	// if (pthread_create(&infosCliente[0], NULL, inserindoDados, &(args->set)) < 0)
	if (pthread_create(&infosCliente[0], NULL, captura_infos, &(args->set)) < 0)
	{
		perror("nao foi possivel criar thread: ");
		return -1;
	}{
		printf("Criou thread 1\n");
	}
	/**** Criando thread para comunicação com o servidor******/
	if (pthread_create(&infosCliente[1], NULL, trataConversa, (void*)args) < 0)
	{
		perror("nao foi possivel criar thread: ");
		return -1;
	}{
		printf("Criou thread 2\n");
	}
	
	while(args->set>=0){}

	//Inicia a leitura do arquivo binário criado
	params_embarc * infos = (params_embarc *) malloc(sizeof(params_embarc)*1000);

    printf("Iniciando a leitura\n");
    int leu = ler_arquivo(infos);
    printf("Quantidade Armazenada: %d\n", leu);
    while(leu-- > 0){
        printf("bateria2: %.2f\n", infos[leu].bateria);
        printf("memoria2: %.2f\n", infos[leu].memoria);
        printf("cpu2: %.2f\n\n", infos[leu].cpu);
    }

	close(socket_desc);
	return 0;
}


// int socket_desc, int *set
void *trataConversa(void *args){
	/*******COMUNICAO - TROCA DE MENSAGENS **************/
	// ptr_thread_arg arg = (thread_arg*) args;
	// printf("Socket %d\n", ((thread_arg*) args)->socket_desc );
	// printf("SET : %d\n", ((thread_arg*) args)->set);
	char *mensagem= (char*) malloc(MAX_MSG*sizeof(char));
	char resposta_servidor[MAX_MSG];
	char *msg = (char*) malloc(MAX_MSG*sizeof(char));
	int tamanho;
	int sleeps = 1; 
	
	//Recebe mensagem de boas vindas
	if((tamanho = recv(((thread_arg*) args)->socket_desc, resposta_servidor, MAX_MSG , 0)) < 0){
		printf("Falha ao receber resposta\n");
		return -1;
	}
	resposta_servidor[tamanho] = '\0';
	
	puts(resposta_servidor);
	int count = 0;
	//Inicializando com o tipo de criptografia
	trocaCrip("##AES256##");

	while(count++ < 60 ){

		//Envia mensagem para o servidor
		//AES 128bits o bloco
		
		strcpy(msg, "Harry James Potter (Godric's Hollow, 31 de julho de 1980) simplesmente Harry Potter é um personagem fictício protagonista da série homônima de livros (e das respectivas adaptações para o cinema) da autora britânica J. K. Rowling. Vai Harry!");
		crip(msg, mensagem);
		printf("Enviando: ... \n");
		fflush(stdout);
		if( send(((thread_arg*) args)->socket_desc , mensagem , strlen(mensagem) , 0) < 0){
			printf("Erro ao enviar mensagem\n");
			return -1;
		}
		fflush(stdin);
		while((tamanho = recv(((thread_arg*) args)->socket_desc , resposta_servidor, MAX_MSG128 , 0)) < 0){
			if(strcmp(resposta_servidor, "##OK##") == 0){
						trocaCrip(msg);
						printf("OK! \n");
			}else{
				fflush(stdout);
				if( send(((thread_arg*) args)->socket_desc , mensagem , strlen(mensagem) , 0) < 0){
					printf("Erro ao enviar mensagem\n");
					// return -1;
				}
			}
		}
		if(!(count%10) && count < 60){
			
			// memset(&msg, '\0', strlen(msg));

			if(count == 10){
				strcpy(msg, "##AES192##");
				msg[strlen(msg)] = '\0';
				// sleeps = 2;
			}else if(count == 20){
				strcpy(msg, "##AES128##");
				msg[strlen(msg)] = '\0';
				// sleeps =4;
			}else if(count == 30){
				strcpy(msg, "##BF128##");
				msg[strlen(msg)] = '\0';
				// sleeps =4;
			}else if(count == 40){
				strcpy(msg, "##BF192##");
				msg[strlen(msg)] = '\0';
				// sleeps =4;
			}else if(count == 50){
				strcpy(msg, "##BF256##");
				msg[strlen(msg)] = '\0';
				// sleeps =4;
			}
			puts(msg);
			memset(mensagem, '\0', MAX_MSG);
			// printf("Mensagem %s\n", mensagem);
			// printf("Vaiiii %s\n", msg);
			crip(msg, mensagem);
			printf("Iniciando o processo de Troca de Criptografia\n");
			fflush(stdout);
			if( send(((thread_arg*) args)->socket_desc , mensagem , strlen(mensagem) , 0) < 0){
				printf("\031[1;42mErro ao enviar mensagem. \031[0m\n");
				// return -1;
			}else{

				//Recebendo resposta do servidor (echo)
				fflush(stdin);
				if( (tamanho = recv(((thread_arg*) args)->socket_desc , resposta_servidor, MAX_MSG128 , 0)) < 0){
					printf("\031[1;42m Falha ao receber resposta \031[0m\n");
					return -1;
				}
				printf("Resposta: ");
				resposta_servidor[tamanho] = '\0';
				puts(resposta_servidor);
				if(strcmp(resposta_servidor, "##TROCAOK##") == 0){
					trocaCrip(msg);
					printf("Processo de Troca finalizada com SUCESSO! \n");
				}else{
					printf("\031[1;42m Processo de Troca NÃO finalizada com SUCESSO! \031[0m\n");
				}

			}

			// //Recebendo resposta do servidor (echo)
			// if( (tamanho = recv(socket_desc, resposta_servidor, MAX_MSG128 , 0)) < 0){
			// 	printf("Falha ao receber resposta\n");
			// 	return -1;
			// }
			// printf("Resposta: ");
			// resposta_servidor[tamanho] = '\0';
			// puts(resposta_servidor);

		}
		//limpa as variaveis
			memset(mensagem, 0, MAX_MSG);
			memset(resposta_servidor, 0, MAX_MSG);
			// memset(msg, '\0', sizeof(msg));
			// free(mensagem);
			// free(resposta_servidor);
			// free(msg);

		sleep(sleeps);
		// //Recebendo resposta do servidor (echo)
		// if( (tamanho = recv(socket_desc, resposta_servidor, MAX_MSG128 , 0)) < 0){
		// 	printf("Falha ao receber resposta\n");
		// 	return -1;
		// }
		// printf("Resposta: ");
		// resposta_servidor[tamanho] = '\0';
		// puts(resposta_servidor);

	}

	((thread_arg*) args)->set=0;
	pthread_exit(NULL);
	
	// return 1;

}