#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "mongoEnv.c"
#define MAX_MSG 56
#define MAX_BLOCK_64 8
#define MAX_BLOCK_128 16
#define MAX_BLOCK_192 24
#define MAX_BLOCK_256 32

/* 
 - Conecta no servidor
 - Espera pela mensagem de boas vindas do servidor
 - Envia sua mensagem
 - Imprime resposta do servidor
 - Finaliza processo do cliente

*/
char **quebraBlocos(char* texto, int numBlocos, int tamanhoBlocos, int tamanhoTexto){
	int count=0;
	int y = 0;
	//char *linhaTemp = malloc(MAX_BLOCK_128*sizeof(char*));
	char **textoTemp= malloc(numBlocos * sizeof(char*));

	printf("%s\n", texto);
	for(int x=0; x < numBlocos ; x++){
		textoTemp[x]= malloc(tamanhoBlocos*sizeof(char));
		for(y=0; y< tamanhoBlocos-1 && count < tamanhoTexto; y++){
			if(y == 0 && (texto[count] == '\0' || texto[count] == ' ')){
				
				count++;
			}
			textoTemp[x][y] = texto[count];
			count++;
		}
		printf("%s\n", textoTemp[x]);
		textoTemp[x][y]='\0';
		// strcpy(textoTemp[x],linhaTemp);
		// memset(&linhaTemp, '\0', sizeof (linhaTemp));
	}
	free(texto);
    return textoTemp;
}

char* arquivo(){
	
	system("echo {\"memoria\" : `smem -m -p - t -c rss | tail -1| sed '1s/%/ /'` , > teste.dat");
	system("upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep 'percentage' | sed 's/ //g' | sed 's/[ ]\\+/ /g' | sed '1s/percentage/\"bateria\" /'| sed '1s/%/ ,/' >> teste.dat ");
	system("NUMCPUS=`grep ^proc /proc/cpuinfo | wc -l`; FIRST=`cat /proc/stat | awk '/^cpu / {print $5}'`; sleep 1; SECOND=`cat /proc/stat | awk '/^cpu / {print $5}'`; USED=`echo 2 k 100 $SECOND $FIRST - $NUMCPUS / - p | dc`; echo \"cpu\": ${USED}} >> teste.dat");
	system("echo `cat teste.dat | tr '\n' ' ' | sed '1s/memoria/\"memoria\" /' | sed '1s/cpu/\"cpu\" /'` > teste.dat");
	FILE *arq;
	char *mensagem = (char *) malloc(MAX_MSG * sizeof(char));
	int result;
	arq = fopen("teste.dat", "rt");
	if (arq == NULL)  // Se houve erro na abertura
	  {
	     printf("Problemas na abertura do arquivo\n");
	     return;
	  }

	result = fread (&mensagem[0], sizeof(char), MAX_MSG, arq);

	fclose(arq);

	return mensagem;
}

void tabelaCriptografias(char *resposta_servidor, char *newCriptografia, int *tamanhoBlocoAtual){
	char tableCrip[4][20] = {"EVP_bf_cbc()", "EVP_aes_128_cbc()", "EVP_aes_192_cbc()", "EVP_aes_256_cbc()"};
	int tamanhoBlocos[3] = {MAX_BLOCK_128, MAX_BLOCK_192, MAX_BLOCK_256};
	int indexCrip =0;
	int indexBloco = 0;
	char* convert;
	// Procura a criptografia e o tamanho de bloco transmitida pelo servidor
 	for (int i = 0; i < 4 && resposta_servidor != NULL; ++i){
	  	if(strstr(resposta_servidor,tableCrip[i]) != NULL)
	  		indexCrip = i;
	  	
		if(i<3){
			sprintf(convert, "%i", tamanhoBlocos[i]);
			if(strstr(resposta_servidor,convert) != NULL)
				indexBloco = i;
	  		}
  	}
  	

  	strcpy(newCriptografia, tableCrip[indexCrip]);
	 //newCriptografia[strlen(newCriptografia)]= '\0';
	*tamanhoBlocoAtual = tamanhoBlocos[indexBloco];
	//tamanhoBlocoAtual[strlen(tamanhoBlocoAtual)] = '\0';
	printf("\033[1;41m %s %d \033[0m\n",newCriptografia, *tamanhoBlocoAtual );
}

int main(int argc , char *argv[]){
	// variaveis
	int socket_desc;
	struct sockaddr_in servidor;
	char *mensagem ;
	char *mensagemTemp ;
	char **textoTemp;
	char *mensagemCifrada ;
	char **mensagemBloco;
	char *fim;
	char resposta_servidor[MAX_MSG];
	int tamanho = 0;
	int numBlocos;
	int restoBlocos;
	int count;
	char *newCriptografia = malloc(20 * sizeof(char));
	int tamanhoBlocoAtual = 0;
	int tamanhoBlocoAtualFIXO = 0;

	/*****************************************/
	/* Criando um socket */
	// AF_INET = ARPA INTERNET PROTOCOLS
	// SOCK_STREAM = TCP
	// 0 = protocolo IP
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
     
	if (socket_desc == -1){
		printf("Nao foi possivel criar socket\n");
		return -1;	
	}

	/* Informacoes para conectar no servidor */
	// IP do servidor
	// familia ARPANET
	// Porta - hton = host to network short (2bytes)
	servidor.sin_addr.s_addr = inet_addr("127.0.0.1");
	servidor.sin_family = AF_INET;
	servidor.sin_port = htons( 1234 );

	//Conectando no servidor remoto
	if (connect(socket_desc , (struct sockaddr *)&servidor , sizeof(servidor)) < 0){
		printf("Nao foi possivel conectar\n");
		return -1;
	}
	printf("Conectado no servidor\n");
	/*****************************************/
	/*******COMUNICACAO - TROCA DE MENSAGENS **************/
	//Recebe mensagem de boas vindas
	if((tamanho = recv(socket_desc, resposta_servidor, MAX_MSG , 0)) < 0){
		printf("Falha ao receber resposta\n");
		return -1;
	}
	resposta_servidor[tamanho] = '\0';
	puts(resposta_servidor);
	//printf("%s\n",resposta_servidor);

	tabelaCriptografias(NULL, newCriptografia, &tamanhoBlocoAtual);
	if(strcmp(newCriptografia, "EVP_bf_cbc()")==0){
		tamanhoBlocoAtualFIXO = MAX_BLOCK_64;
	}else{
		tamanhoBlocoAtualFIXO = MAX_BLOCK_128;   
	}
	//Envia mensagem para o servidor
	while (resposta_servidor != "fim"){

		//Captura dos dados a serem transmitidos
		mensagem = arquivo();

		//Inicio do processo de quebra dos blocos de dados
		numBlocos = (int) strlen(mensagem)/(tamanhoBlocoAtualFIXO-1);
		restoBlocos = (int) strlen(mensagem)%(tamanhoBlocoAtualFIXO-1);
		
		if(restoBlocos > 0)
			numBlocos++;

		//[numBlocos][MAX_BLOCK_128];
		//quebraBlocos(char* texto,int numBlocos, int tamanhoBlock, int tamanhoTexto)
		textoTemp=quebraBlocos(mensagem, numBlocos, tamanhoBlocoAtualFIXO, strlen(mensagem));
		//printf("%s\n", textoTemp[0]);
		count=0;
		while(count < numBlocos && textoTemp[0] != NULL){
			printf("Entrou\n");
			mensagemCifrada = initCrip(textoTemp[count],tamanhoBlocoAtual, newCriptografia);

			//imprimeTextoCifrado(mensagemCifrada);
			if(mensagemCifrada != NULL || mensagemCifrada != " " || mensagemCifrada[0] != '\0'){
				if( send(socket_desc , mensagemCifrada , strlen(mensagemCifrada) , 0) < 0){
					printf("Erro ao enviar mensagem\n");
					return -1;
				}
				memset(&mensagemCifrada, '\0', sizeof(mensagemCifrada));
				free(mensagemCifrada);
				//free(mensagem);
				//limpa as variaveis
				//memset(&mensagem, '\0', sizeof (mensagem));
				count++;
				
				while( (tamanho = recv(socket_desc, resposta_servidor, MAX_MSG , 0)) < 0){
				}
				if(tamanho > 0){
					printf("Resposta: ");
					resposta_servidor[tamanho] = '\0';
					puts(resposta_servidor);

					if(strstr(resposta_servidor, "Iniciar processo de troca: ") != NULL){

						tabelaCriptografias(resposta_servidor, newCriptografia, &tamanhoBlocoAtual);
						if(strcmp(newCriptografia, "EVP_bf_cbc()") == 0){
						    tamanhoBlocoAtualFIXO = MAX_BLOCK_64;
						}else{
						    tamanhoBlocoAtualFIXO = MAX_BLOCK_128;   
						}

						printf("Iniciando nova criptografia\n");

						mensagemCifrada = initCrip("Finalizada." ,tamanhoBlocoAtual, newCriptografia);
						mensagemCifrada[strlen(mensagemCifrada)] = '\0';
							if( send(socket_desc , mensagemCifrada, strlen(mensagemCifrada) , 0) < 0){
								printf("Erro ao enviar mensagem\n");
								return -1;
							}
							memset(&mensagemCifrada, '\0', sizeof(mensagemCifrada));
							free(mensagemCifrada);

							while( (tamanho = recv(socket_desc, resposta_servidor, MAX_MSG , 0)) < 0){}
							if(tamanho > 0){
								printf("Resposta: ");
								resposta_servidor[tamanho] = '\0';
								puts(resposta_servidor);
							}
							
							

					}
				}
			}else{

				printf("Erro ao cifrar o bloco\n");
				
				memset(&mensagemCifrada, '\0', sizeof(mensagemCifrada));
				free(mensagemCifrada);

			}
			sleep(5);
		}

		// //Recebendo resposta do servidor (echo)
		// if( (tamanho = recv(socket_desc, resposta_servidor, MAX_MSG , 0)) < 0){
		// 	printf("Falha ao receber resposta\n");
		// 	return -1;
		// }
		// printf("Resposta: ");
		// resposta_servidor[tamanho] = '\0';
		// puts(resposta_servidor);

		memset(&resposta_servidor, '\0', sizeof (resposta_servidor));
		//memset(&resposta_servidor, '\0', sizeof (resposta_servidor));
		
	}
	/*****************************************/
	close(socket_desc);

	printf("Cliente finalizado com sucesso!\n");
	return 0;
}
