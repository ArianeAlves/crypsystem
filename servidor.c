#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "mongoEnv.c"
#define MAX_MSG 56
#define MAX_BLOCK_64 8
#define MAX_BLOCK_128 16
#define MAX_BLOCK_192 24
#define MAX_BLOCK_256 32


/* 
	Servidor ECHO multithread

 - Espera cliente conectar
 - Cria nova thread para o cliente
 - Envia mensagem de boas vindas para o cliente
 - Espera mensagem do cliente
 - Envia a mensagem de echo para o cliente
*/

void *tratador_conexao(void *);
void tabelaCriptografias(int , int , char *, int *);
int timerUltimaTroca(time_t, time_t *);
void troca_crip(void* , char* , int );


// void logInfo(char* texto){
// 	char command[MAX_MSG];
// 	// strcat(command, "echo '");
// 	// strcat(command, texto);
// 	// strcat(command, "'  >> logServidor.txt");
// 	// system(command);
// 	// memset(&command, '\0', sizeof (command));
// 	//| sed 's/.*%/&\\n/g'
//     FILE *arq;
//     int result;
//     int i;
//     arq = fopen("logServidor1.txt", "a"); // Cria um arquivo binário para gravação
//     system("chmod 777 logServidor.txt");
    
//     if (arq == NULL) 
//     {// Se não conseguiu criar
//         printf("Problemas na CRIACAO do arquivo\n");

//    	return;
//     }
//     result = fprintf(arq , texto, NULL);
//     if( result> 0) { 
//         printf("Erro na escrita do arquivo");
//     	return;
//    	}
//     //printf("%s\n", texto);
//     // result = ;
//     fclose(arq);
// }

void *tratador_conexao(void *conexao){
	int sock = *(int *)conexao;
	int tamanho;
	int fim = 0;
	char recebida[MAX_MSG];
	char *newRecebida;
	char *mensagem;
	char *concatena = (char*) malloc(MAX_MSG*sizeof(char));
	time_t inicio;
	char* status;
	int tamanhoBlocoAtual = 0;
	char *newCriptografia = (char*) malloc(20*sizeof(char));
	char *ultimoStatus;
	int indexCountCrip = 0;
	int indexCountTamanho = 0;
	ultimoStatus = "bom";
	//Enviando mensagem para o cliente
	mensagem = "Servidor> Bem vindo.";
	//mensagem[sizeof(mensagem)] = '\0';
	printf("%s\n", mensagem);
	write(sock, mensagem, strlen(mensagem));

	time(&inicio);
	//inicio=clock();
	printf("Timer inicial: %ld\n", inicio);
	status = verificaRecursos();
	tabelaCriptografias(0, 0, newCriptografia, &tamanhoBlocoAtual);
	recebida[0]='\0';
	while ((tamanho = recv(sock, recebida, tamanhoBlocoAtual, 0)) > 0)
	{	printf("Entrou\n");
		recebida[tamanho] = '\0';
		imprimeTextoCifrado(recebida);
		newRecebida = initDecrip(recebida,tamanho, tamanhoBlocoAtual, newCriptografia);
		printf("Depois: %s\n", newRecebida);
		//newRecebida[tamanho] = '\0';
		
		for(int i=0; i<tamanho;i++){
			if(newRecebida[i] == '}')
				fim = 1;
		}

		// adicionando ao mongoDB
		if(fim == 1){
			strcat(concatena, newRecebida);
			if(inicioInsert(concatena) == 0){
				printf("Ok Server\n");
				fim =0;
				//concatena = (char*) calloc(MAX_MSG, sizeof(char));
				strcpy(concatena, "\0");
				
				status = verificaRecursos();
				if(status != NULL){
					printf("%s\n", status);
				    if(status == "bom" && timerUltimaTroca(inicio, &inicio)==1){
				    	printf("Primeira mudança\n");
				      if(ultimoStatus == "ruim" && indexCountCrip >0 && indexCountCrip <3
				        && indexCountTamanho > 0 && indexCountTamanho <2){
				          indexCountCrip--;
				          indexCountTamanho--;
				        }else if(ultimoStatus == "bom" && indexCountCrip >0 && indexCountCrip <3
				          && indexCountTamanho > 0 && indexCountTamanho < 2){
				          indexCountCrip++;
				          indexCountTamanho++;
				        }
				        tabelaCriptografias(indexCountCrip, indexCountTamanho, newCriptografia, &tamanhoBlocoAtual);
				        troca_crip(conexao, newCriptografia, tamanhoBlocoAtual);
				        
				      }else{
				      	
				        if(timerUltimaTroca(inicio, &inicio)==1){
				          if(strcmp(newCriptografia, "EVP_bf_cbc()") == 0 && tamanhoBlocoAtual != MAX_BLOCK_256){
				            indexCountTamanho++;
				          }else if(strcmp(newCriptografia, "EVP_bf_cbc()") == 0){
				              indexCountCrip = 1;
				              indexCountTamanho = 0;
				          }else if(strcmp(newCriptografia, "EVP_aes_128_cbc()") == 0){
				              indexCountCrip = 2;
				              indexCountTamanho = 1;
				          }else if(strcmp(newCriptografia, "EVP_aes_192_cbc()") == 0){
				              indexCountCrip = 3;
				              indexCountTamanho = 2;
				          }else if(strcmp(newCriptografia, "EVP_aes_256_cbc()") == 0){
				              indexCountCrip = 0;
				              indexCountTamanho = 0;
				          }
				        tabelaCriptografias(indexCountCrip, indexCountTamanho, newCriptografia, &tamanhoBlocoAtual);
				        troca_crip(conexao, newCriptografia, tamanhoBlocoAtual); 
				        
				      }else
				      	write(sock, "Ok Server", strlen("Ok Server"));
				      
				    }
				    printf("Passei\n");
				    ultimoStatus = status;
				    status="\0";
				    printf("Passei\n");
				}else{
					write(sock, "Ok Server", strlen("Ok Server"));
				}
				//free(concatena);
				//write(sock, "Ok recebida", strlen("Ok recebida"));

			}
				
		}else if(strcmp(newRecebida,"Finalizada.") == 0){

			write(sock, "Ok recebida", strlen("Ok recebida"));
			time(&inicio);

		}
		else{
			strcat(concatena, newRecebida);
			printf("%s\n", concatena );
			// Responde
			write(sock, "Ok recebida", strlen("Ok recebida"));
		}
		memset(&recebida, '\0', sizeof (recebida));
		memset(&newRecebida, '\0', sizeof (newRecebida));
		//free(recebida);
		free(newRecebida);
		// devolvendo a mensagem
		//write(sock, recebida, strlen(recebida));
	}

	if (tamanho == 0)
	{
		puts("Cliente desconectou.\n");
		
		strcpy(concatena, "\0");

		fflush(stdout);
	}
	else if (tamanho == -1)
	{
		perror("erro no recebimento: ");
	}
	//Liberando o ponteiro
	free(conexao);

	return 0;
}

void tabelaCriptografias(int indexCrip, int indexBloco, char *newCriptografia, int *tamanhoBlocoAtual){
  char tableCrip[4][20] = {"EVP_bf_cbc()", "EVP_aes_128_cbc()", "EVP_aes_192_cbc()", "EVP_aes_256_cbc()"};
  int tamanhoBlocos[3] = {MAX_BLOCK_128, MAX_BLOCK_192, MAX_BLOCK_256};
  strcpy(newCriptografia, tableCrip[indexCrip]);
  //newCriptografia[strlen(newCriptografia)]= '\0';
  *tamanhoBlocoAtual = tamanhoBlocos[indexBloco];
  printf("Tamanho do Bloco atual:%d\n Criptografia: %s \n", *tamanhoBlocoAtual, newCriptografia);
  //tamanhoBlocoAtual[strlen(tamanhoBlocoAtual)] = '\0';
}

int timerUltimaTroca(time_t troca, time_t *fim){
	time_t now, gasto;
	time(&now);

	gasto = now - troca;
	printf("Timer: %ld/ inicio: %ld/ agora: %ld\n", gasto, troca, now);
	//Verificação a cada 1 min = 60 segundos
	if(gasto > 60){
	    //time(fim);
	    printf("Inicio da Troca\n");
	    return 1;
	}
	  return 0;
}

void troca_crip(void *conexao, char* newCriptografia, int tamanhoBlocoAtual){
  int sock = *(int*)conexao;
  char concatena[256];
  char *recebida;
  char *buffer;
  int tamanho;
  // strcat(concatena, "Iniciar processo de troca: ");
  // strcat(concatena, newCriptografia);
  // sprintf(buffer, "%d", tamanhoBlocoAtual);
  // strcat(concatena, buffer);
  sprintf( concatena, "Iniciar processo de troca: %s %d", newCriptografia, tamanhoBlocoAtual);
  concatena[strlen(concatena)] = '\0';
  //printf("Teste1: %s\n", concatena);
  write(sock,concatena, strlen(concatena));
  while ((tamanho = recv(sock, recebida, tamanhoBlocoAtual, 0)) > 0);

  if (recebida == "Troca Finalizada")
  {
  	printf("Troca realizada com Sucesso\n");
  }
  strcpy(concatena, "\0");
}

int main(void)
{                
	//variaveis
	int socket_desc, conexao, c, *nova_conexao;
	struct sockaddr_in server, client;
	char *mensagem;
	char resposta[MAX_MSG];
	int nbytes, count;

	// para pegar o IP e porta do cliente
	char *client_ip;
	int client_port;

	/*********************************************************/
	//Criando um socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1)
	{
		printf("Nao foi possivel criar o socket\n");
		return -1;
	}

	int reuso = 1;
	if (setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuso, sizeof(reuso)) < 0)
	{
		perror("Não foi possível reusar endereço");
		return -1;
	}
#ifdef SO_REUSEPORT
	if (setsockopt(socket_desc, SOL_SOCKET, SO_REUSEPORT, (const char *)&reuso, sizeof(reuso)) < 0)
	{
		perror("Não foi possível reusar porta");
		return -1;
	}
#endif

	//Preparando a struct do socket
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY; // Obtem IP do S.O.
	server.sin_port = htons(1234);

	//Associando o socket a porta e endereco
	if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
	{
		puts("Erro ao fazer bind\n");
	}
	puts("Bind efetuado com sucesso\n");

	// Ouvindo por conexoes
	listen(socket_desc, 3);

	/*********************************************************/
	//Aceitando e tratando conexoes
	puts("Aguardando por conexoes...");
	c = sizeof(struct sockaddr_in);
	// Fica esperando por conexoes
	while ((conexao = accept(socket_desc, (struct sockaddr *)&client, (socklen_t *)&c)))
	{
		if (conexao < 0)
		{
			perror("Erro ao receber conexao\n");
			return -1;
		}
		// pegando IP e porta do cliente
		client_ip = inet_ntoa(client.sin_addr);
		client_port = ntohs(client.sin_port);
		printf("Cliente conectou: %s : [ %d ]\n", client_ip, client_port);

		/**** Criando thread para tratar da comunicacao ******/
		pthread_t processo;
		nova_conexao = malloc(1);
		*nova_conexao = conexao;

		if (pthread_create(&processo, NULL, tratador_conexao, (void *)nova_conexao) < 0)
		{
			perror("nao foi possivel criar thread: ");
			return -1;
		}

	} //fim do while

	/*********************************************************/

	close(socket_desc);
	shutdown(socket_desc, 2);

	printf("Servidor finalizado...\n");
	return 0;
}
